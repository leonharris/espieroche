<?php

/* custom theme functions here  */

// Register theme setup
require_once(get_stylesheet_directory().'/inc/functions/setup.php');

// Register scripts and stylesheets
require_once(get_stylesheet_directory().'/inc/functions/enqueue-scripts.php');

// Register Bootstrap 4 navwalker
// to enable WP menu to play wiht Bootstrap 4
// this custom version adds post thumbnails to 2nd level (i.e. dropdown) menu items
// in this case used to show product images
require_once(get_stylesheet_directory().'/inc/functions/custom_bs4navwalker.php');

// Register menus
require_once(get_stylesheet_directory().'/inc/functions/menus.php');

// Register widgets
require_once(get_stylesheet_directory().'/inc/functions/widgets.php');


// Theme functions
require_once(get_stylesheet_directory().'/inc/functions/theme-functions.php');

// Plugin functions

// WooCommerce functions
require_once(get_stylesheet_directory().'/inc/functions/plugins/woocommerce.php');

// Advanced Custom Fields functions
require_once(get_stylesheet_directory().'/inc/functions/plugins/acf.php');

// Yoast SEO functions
require_once(get_stylesheet_directory().'/inc/functions/plugins/yoast-seo.php');
