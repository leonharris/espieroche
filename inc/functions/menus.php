<?php

/*  Registrer menus. */
register_nav_menus( array(
	'primary-left' => __( 'Main Menu Left', 'espieroche' ),
	'primary-right' => __( 'Main Menu Right', 'espieroche' ),
	//'primary_mobile' => __( 'Main Menu - Mobile', 'espieroche' ),
	'social' => __( 'Footer Social Links Menu', 'espieroche' ),
	'footer' => __( 'Footer Menu', 'espieroche' ),
	//'helper_left' => __( 'Helper Menu left', 'espieroche' ),
	//'helper_right' => __( 'Helper Menu right', 'espieroche' ),
	//'my_account' => __( 'My Account Menu', 'espieroche' ),
//	'product_links' => __( 'Product Links', 'espieroche' ),
) );
