<?php




//
// WooCommerce Shared functions
// ------------------------------

// enable the product image gallery features
add_action( 'after_setup_theme', 'add_er_theme_support', 100 );

function add_er_theme_support() {
	//add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

/*
// disable the product image gallery features
add_action( 'after_setup_theme', 'remove_er_theme_support', 100 );

function remove_er_theme_support() {
	remove_theme_support( 'wc-product-gallery-zoom' );
	remove_theme_support( 'wc-product-gallery-lightbox' );
	remove_theme_support( 'wc-product-gallery-slider' );
}
*/




/*
 * Manage WooCommerce styles and scripts.
*/
function grd_woocommerce_script_cleaner() {

	// Remove the generator tag
	remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

	// Unless we're in the store, remove all the cruft!
	if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
		wp_dequeue_style( 'woocommerce-smallscreen' );
		wp_dequeue_style( 'woocommerce_fancybox_styles' );
		wp_dequeue_style( 'woocommerce_chosen_styles' );


		wp_dequeue_script( 'selectWoo' );
		wp_deregister_script( 'selectWoo' );
		wp_dequeue_script( 'wc-add-payment-method' );
		wp_dequeue_script( 'wc-lost-password' );
		wp_dequeue_script( 'wc_price_slider' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-add-to-cart' );
		wp_dequeue_script( 'wc-cart-fragments' );
		wp_dequeue_script( 'wc-credit-card-form' );
		wp_dequeue_script( 'wc-checkout' );
		wp_dequeue_script( 'wc-add-to-cart-variation' );
		wp_dequeue_script( 'wc-single-product' );
		wp_dequeue_script( 'wc-cart' );
		wp_dequeue_script( 'wc-chosen' );
		wp_dequeue_script( 'woocommerce' );
		wp_dequeue_script( 'jquery-blockui' );
		wp_dequeue_script( 'jquery-placeholder' );
		wp_dequeue_script( 'jquery-payment' );
		wp_dequeue_script( 'fancybox' );
		wp_dequeue_script( 'jqueryui' );
	}

	// Dequeue these on ALL site pages inc. Woo ones
	wp_dequeue_style( 'woocommerce-layout' );
	wp_dequeue_style( 'woocommerce-general');
	wp_dequeue_style( 'woocommerce_frontend_styles' );
	wp_dequeue_script( 'prettyPhoto' );
	wp_dequeue_script( 'prettyPhoto-init' );
	wp_dequeue_style( 'woocommerce_prettyPhoto_css' );

}
add_action( 'wp_enqueue_scripts', 'grd_woocommerce_script_cleaner', 99 );


// Modify Woo opening and closing divs

remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

add_action('woocommerce_before_main_content', 'my_theme_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'my_theme_wrapper_end', 10);

function my_theme_wrapper_start() {
  echo '<div class="wrap">
  	<div id="primary" class="content-area">
  		<main id="main" class="site-main">';
}

function my_theme_wrapper_end() {
  echo '</main></div></div>';
}


// Remove breadcrumbs from all Woo pages
add_filter( 'woocommerce_before_main_content', 'remove_breadcrumbs');
function remove_breadcrumbs() {
	remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
}


// Remove the sorting dropdown from Woocommerce
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_catalog_ordering', 30 );
// Remove the result count from WooCommerce
remove_action( 'woocommerce_before_shop_loop' , 'woocommerce_result_count', 20 );



// Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php).
// Used in conjunction with https://gist.github.com/DanielSantoro/1d0dc206e242239624eb71b2636ab148
// Compatible with WooCommerce 3.0+. Thanks to Alex for assisting with an update!

function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a class="cart-customlocation" href="<?php echo esc_url(wc_get_cart_url()); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>"><?php echo sprintf(_n('Cart: %d', 'Cart: %d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></a>
	<?php
	$fragments['a.cart-customlocation'] = ob_get_clean();
	return $fragments;
}



// Change ‘In Stock’ / ‘Out of Stock’ text displayed on a product page

add_filter( 'woocommerce_get_availability', 'wcs_custom_get_availability', 1, 2);
function wcs_custom_get_availability( $availability, $_product ) {
	/*
    // Change In Stock Text
    if ( $_product->is_in_stock() ) {
        $availability['availability'] = __('Available!', 'woocommerce');
    } */
    // Change Out of Stock Text
    if ( ! $_product->is_in_stock() ) {
        $availability['availability'] = __('Coming soon', 'woocommerce');
    }
    return $availability;
}



//
// Main Shop Archive page
// ------------------------------


function custom_pre_get_posts_query( $q ) {

	if ( ! is_admin() // only run if not admin
		&& $q->is_main_query() // only run if main query
		//&& ( $q->is_shop() || $q->is_product_category() || $q->is_post_type_archive( 'product' ) ) // only run if /shop or shop cat archive
		&& $q->is_post_type_archive( 'product' ) // only run if Product archive
	) :

		// remove bespoke category from main woo loop
		$taxquery = array(
	        array(
	            'taxonomy' => 'product_cat',
	            'field' => 'slug',
	            'terms' => array( 'bespoke' ),
	            'operator'=> 'NOT IN'
	        )
	    );

		$q->set('tax_query', $taxquery); //  remove bespoke product category from loop

		$q->set('posts_per_page', -1);
        //$q->set('post_type', array('product_variation'));

		// Add Variations into WooCommerce loop
		//add_filter( 'posts_where', 'rc_filter_where' );

		return;

	endif;

}
add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' );


// Add the_content to main Shop Archive page
// outputs after main product loop
// and also gets any custom flexible fields
add_action( 'woocommerce_after_main_content', 'additional_content_in_shop', 25 );
function additional_content_in_shop() {
    // Only on "shop" archives pages
    if( ! is_shop() ) return;

	$shop_page_id = wc_get_page_id( 'shop' ); // get the current shop page ID

	// get the_content post
	$content_post = get_post($shop_page_id);
	$content = $content_post->post_content;
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	// if we have content display it
	if ($content) {
		echo '<div class="">' . $content . '</div>';
	}

	// check if the flexible content field has rows of data
	if( have_rows('flexible_content', $shop_page_id) ):

		// loop through the rows of data
		while ( have_rows('flexible_content', $shop_page_id) ) : the_row();

			if( get_row_layout() == 'two_img_and_txt' ):

				get_template_part( 'components/flexible-content/flex', 'two-images-and-text' );

			elseif( get_row_layout() == 'three_img_and_txt' ):

				get_template_part( 'components/flexible-content/flex', 'three-images-and-text' );

			elseif( get_row_layout() == 'img_grid' ):

				get_template_part( 'components/flexible-content/flex', 'image-grid' );

			elseif( get_row_layout() == 'full_width_img' ):

				get_template_part( 'components/flexible-content/flex', 'image-full-width' );

			elseif( get_row_layout() == 'quote' ):

				get_template_part( 'components/flexible-content/flex', 'quote' );

			endif;

		endwhile;

	else :

		// no layouts found

	endif;



}


// Use the Featued image at the top of the main shop archive page

add_action('woocommerce_before_main_content', 'wp176545_add_feature_image', 1);

function wp176545_add_feature_image() {
	// Only on "shop" archive pages
	if( ! is_shop() ) return;
		echo get_the_post_thumbnail( get_option( 'woocommerce_shop_page_id' ) );
}

/*
// remove sidebar shop page woocommerce
add_action('woocommerce_before_main_content', 'remove_sidebar' );
function remove_sidebar() {
   if ( is_shop() ) {
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
  }
}
*/

/**
 * remove add to cart buttons on shop archive page
 */

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);


//
// Single Product
// ------------------------------


/**
 *  Remove Sidebar @ Single Product Page
 */

add_action( 'wp', 'bbloomer_remove_sidebar_product_pages' );

function bbloomer_remove_sidebar_product_pages() {
	if ( is_product() ) {
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
	}
}


// Add Custom Content After 'Add To Cart' Button On Product Page
add_action( 'woocommerce_after_add_to_cart_button', 'content_after_addtocart_button' );

function content_after_addtocart_button() {
	// add social share buttons
	get_template_part( 'inc/social', 'share' );
}

// Remove Related Products Output
// And add above Add to Cart button
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action( 'woocommerce_before_add_to_cart_button', 'woocommerce_output_related_products', 30 );


// Remove Product Category
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );



// Remove the following from product tabs
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['description'] );      	// Remove the description tab
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}



// Move Description below price
function woocommerce_template_product_description() {
	echo '<div class="product_description">';
	wc_get_template( 'single-product/tabs/description.php' );
	echo '</div>';
}
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_product_description', 20 );




//
// Shopping Bag
// ------------------------------


// Remove attribute from Cart title
// Add re-add in own <div> below
// https://nicola.blog/2017/06/01/remove-attribute-from-product-title/
add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );




//
// Checkout
// ------------------------------

// Change The PayPal Icon In WooCommerce
// http://www.codemyownroad.com/change-paypal-icon-woocommerce/
function replacePayPalIcon($iconUrl) {
	return get_bloginfo('stylesheet_directory') . '/images/acceptedCards.png';
}

add_filter('woocommerce_paypal_icon', 'replacePayPalIcon');


// Change WooCommerce 'Ship to a different address?' Default State
// https://metorik.com/blog/change-woocommerce-ship-to-a-different-address-default
add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );


/**
 * Removes the attribute from the product title, in the cart.
 *
 * @return string
 */
function remove_variation_from_product_title( $title, $cart_item, $cart_item_key ) {
	$_product = $cart_item['data'];
	$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

	if ( $_product->is_type( 'variation' ) ) {
		if ( ! $product_permalink ) {
			return $_product->get_title();
		} else {
			return sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() );
		}
	}

	return $title;
}
add_filter( 'woocommerce_cart_item_name', 'remove_variation_from_product_title', 10, 3 );



// Modify Order of Checkout fields

add_filter("woocommerce_checkout_fields", "custom_order_fields");

function custom_order_fields($fields) {

	/*
	$order = array(
		"billing_email",
		"billing_first_name",
		"billing_last_name",
		"billing_phone",
		"billing_address_1",
		"billing_postcode",
		"billing_address_2",
		"billing_country"
	);

	foreach($order as $field) {
		$ordered_fields[$field] = $fields["billing"][$field];
	}

	$fields["billing"] = $ordered_fields;*/

	$fields['billing']['billing_phone']['priority'] = 21;
	$fields['billing']['billing_email']['priority'] = 22;

	return $fields;
}
