<?php

// How to Add Content to a Site’s Footer without Modifying Your Theme
add_action('wp_footer', 'wpshout_action_example');
function wpshout_action_example() {
	// add cookie bar to site footer
    echo '<div id="cookie-consent-bar">
		<div class="er-inner">
			<span class="er-left-side">This site uses cookies:  <a class="er-more-info-link" tabindex=0 target="_blank" href="' . get_site_url() . '/cookie-policy/">Find out more.</a></span>
			<span class="er-right-side">
				<button class="btn" id="cookieConsent" tabindex="0">Okay, thanks</button>
			</span>
		</div>
	</div><!-- #cookie-consent-bar -->';
}


// Add Page Slug Body Class
function add_slug_body_class( $classes ) {

    global $post;

	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}

    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


/*
add_filter( 'wp_get_attachment_image_attributes', 'gs_change_attachment_image_markup' );
/**
 * Change src and srcset to data-src and data-srcset, and add class 'lazyload'
 * @param $attributes
 * @return mixed

function gs_change_attachment_image_markup($attributes){

    if (isset($attributes['src'])) {
        $attributes['data-src'] = $attributes['src'];
        $attributes['src'] = ''; //could add default small image or a base64 encoded small image here
    }
    if (isset($attributes['srcset'])) {
        $attributes['data-srcset'] = $attributes['srcset'];
        $attributes['srcset'] = '';
    }
    $attributes['class'] .= ' lazyload';
    return $attributes;

}*/
