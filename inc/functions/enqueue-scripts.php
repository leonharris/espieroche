<?php


// Add JS Scripts and CSS Styles to theme


function site_scripts() {

  	global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

	// jQuery first, then popper, then Bootstrap JS.
  	wp_deregister_script('jquery');
  	wp_register_script('jquery', "//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js", true, null);
  	wp_enqueue_script('jquery');

    // Add Bootstrap JS

    wp_enqueue_script( 'popper', '//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array(), null, true ); // using a CDN for this as it looks like they might remove in later Bootstrap versions

	wp_enqueue_script( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array(), null, true );
	// bootstrap js now added by theme (compiled individual plugins loaded)
	//wp_enqueue_script( 'bootstrap-custom-js', get_template_directory_uri() . '/assets/js/bootstrap-custom.min.js', array( 'jquery' ), null, true );

    // Adding scripts file in the footer
	wp_enqueue_script( 'vendor-js', get_template_directory_uri() . '/js/vendor.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'custom-js', get_template_directory_uri() . '/js/custom.min.js', array( 'vendor-js' ), null, true );

	// CSS

	// Font Awesome
	wp_register_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), null );
	wp_enqueue_style( 'font-awesome' );

    // Register main stylesheet
    wp_enqueue_style( 'site-css', get_template_directory_uri() . '/style.css', array(), null, 'all' );


}
add_action('wp_enqueue_scripts', 'site_scripts', 999);



/*
function espieroche_google_analytics() { ?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113127980-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-113127980-1');
	</script>
<?php }
add_action( 'wp_head', 'espieroche_google_analytics', 10 );
*/
