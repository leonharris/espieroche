<?php
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function espieroche_setup() {

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	// Remove Emoji Styles & Scripts
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );

	// Add WP Thumbnail Support
	add_theme_support( 'post-thumbnails' );

	// Set default image sizes
	// these override those set in wp-admin
	//add_image_size( 'medium', 767, 479, true );

	// proportional sizes 6/4 ratio
	//add_image_size( 'thumbnail', 420, 280, true ); set in wp-admin
	add_image_size( '16_9_medium', 767, 511, true );
	add_image_size( '16_9_large', 1200, 800, true );
	add_image_size( '16_9_xlarge', 1600, 1067, true );
	add_image_size( '16_9_xxlarge', 2500, 1667, true );

	add_image_size( 'square-xsmall', 100, 100, true  ); // force crop
	add_image_size( 'square-small', 300, 300, true  ); // force crop
	add_image_size( 'square-medium', 600, 600, true  ); // force crop
	add_image_size( 'square-large', 1200, 1200, true  ); // force crop

	// Add RSS Support
	add_theme_support( 'automatic-feed-links' );

	// Add Video header support
	add_theme_support( 'custom-header', array(
	 'video' => true,
	) );

	// Add HTML5 Support
	add_theme_support( 'html5',
			 array(
				'search-form',
			 )
	);

	// declare woocommerce support

	add_theme_support( 'woocommerce', array(
		'gallery_thumbnail_image_width' => 150, // set gallery image width

	) );


	// Add excerpts to Pages
	add_post_type_support( 'page', 'excerpt' );

}

add_action( 'after_setup_theme', 'espieroche_setup' );

/*
// Add Favicons to your WordPress
function blog_favicon() { ?>

	<link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicons/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicons/manifest.json">
	<link rel="mask-icon" href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">

<?php }
add_action('wp_head', 'blog_favicon'); */
