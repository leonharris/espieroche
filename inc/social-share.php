<?php
// social share code
$encoded_title = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');
$permalink = esc_url(get_the_permalink());
$excerpt = rawurlencode(get_the_excerpt());
?>

<script>

	function PopupCenter(url, title, w, h) {
	    // Fixes dual-screen position                         Most browsers      Firefox
	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

	    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	    var top = ((height / 2) - (h / 2)) + dualScreenTop;
	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	    // Puts focus on the newWindow
	    if (window.focus) {
	        newWindow.focus();
	    }
	}

	function tweetIt() {
		url="<?php echo get_the_permalink(get_the_ID()); ?>";
		via="";
		text="<?php echo $encoded_title ?>";
		PopupCenter('https://twitter.com/intent/tweet?url='+encodeURIComponent(url)+'&via=espieroche'+encodeURIComponent(via)+'&text='+text,'tweet',600,300);
		return false;
	}


	function fbIt() {
		u="<?php echo get_the_permalink(get_the_ID()); ?>";
		t="<?php echo $encoded_title; ?>";
		PopupCenter('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer',600,436);
		return false;
	}

	function pinIt() {
	  var e = document.createElement('script');
	  e.setAttribute('type','text/javascript');
	  e.setAttribute('charset','UTF-8');
	  e.setAttribute('src','https://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);
	  document.body.appendChild(e);
	}

</script>

<div class="social-share-woo">

	<a class="twitter" href="javascript:tweetIt();"><i class="fa fa-twitter"></i></a>

	<a class="fb_link facebook" href="javascript:fbIt();"><i class="fa fa-facebook"></i></a>

	<a href="javascript:pinIt();" class="pinterest pin-it"><i class="fa fa-pinterest"></i></a>

</div>
