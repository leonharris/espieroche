<?php
/**
 * The template for displaying all single posts
 *
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					get_template_part( 'components/post/content', get_post_format() );

					/*
					the_post_navigation( array(
						'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'espieroche' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'espieroche' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . espieroche_get_svg( array( 'icon' => 'previous' ) ) . '</span>%title</span>',
						'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'espieroche' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'espieroche' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . espieroche_get_svg( array( 'icon' => 'next' ) ) . '</span></span>',
					) ); */

				endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	<?php get_sidebar(); ?>
</div><!-- .wrap -->

<?php get_footer();
