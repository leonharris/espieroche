<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="wrap">
			<?php
			get_template_part( 'components/footer/footer', 'widgets' );

			if ( has_nav_menu( 'footer' ) ) : ?>
				<nav class="footer-navigation" aria-label="<?php _e( 'Footer Menu', 'espieroche' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'footer',
							'menu_class'     => 'footer-menu',
							'depth'          => 1,
						) );
					?>
				</nav><!-- .social-navigation -->
			<?php endif;

			get_template_part( 'components/footer/site', 'info' );
			?>
		</div><!-- .wrap -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
