<?php

// Hero image component
// For displaying Featured image at the top of Pages and Posts

$small_img = get_the_post_thumbnail_url(get_the_ID(),'16_9_medium' ); // 767 width 6/4 ratio
$medium_img = get_the_post_thumbnail_url(get_the_ID(),'16_9_large' ); // 6/4 ratio
$xlarge_img = get_the_post_thumbnail_url(get_the_ID(),'16_9_xlarge' ); // 6/4 ratio
$xxlarge_img = get_the_post_thumbnail_url(get_the_ID(),'16_9_xxlarge' ); // 6/4 ratio

$thumb_id = get_post_thumbnail_id(get_the_ID());
$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);
?>
<div class="single-hero-banner">
	<div class="hero-image">
		<noscript>
			<img src="<?php echo esc_url($medium_img); ?>" />
		</noscript>
		<img
		data-sizes="auto"
		src="<?php echo esc_url($small_img); ?>"
		data-srcset="<?php echo esc_url($small_img); ?> 767w,
		<?php echo esc_url($medium_img); ?> 1200w,
		<?php echo esc_url($xlarge_img); ?> 1600w,
		<?php echo esc_url($xxlarge_img); ?> 2500w"
		class="lazyload blur-up"
		alt="<?php echo $alt; ?>" />
	</div>
</div>
