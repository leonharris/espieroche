<?php
// Code for Displaying Hero Video

// get iframe HTML
$iframe = get_field('featured_video');


// use preg_match to find iframe src
preg_match('/src="(.+?)"/', $iframe, $matches);
$src = $matches[1];


// add extra params to iframe src
$params = array(
    'controls'    => 0,
    'hd'        => 1,
	'fs'	=> 0, // 0 prevents the fullscreen button from displaying in the player.
    'autohide'    => 1,
	'autoplay' => 1,
	'showinfo' => 0,
	'loop' => 0, // 0 = don't loop
	'rel' => 0, //  whether the player should show related videos when playback of the initial video ends
	'modestbranding' => 1,
);

$new_src = add_query_arg($params, $src);

$iframe = str_replace($src, $new_src, $iframe);

// add extra attributes to iframe html
$attributes = 'frameborder="0"';

$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);

echo '<div class="embed-container">' . $iframe . '</div>';
