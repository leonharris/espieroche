<?php
$images = get_field('slider');
$size = '16_9_xxlarge'; // (thumbnail, medium, large, full or custom size)

if( $images ):
$i = 1; ?>
<div class="single-hero-slider">
	<?php foreach( $images as $image ): ?>
		<div class="hero-image hero-image-<?php echo $i; ?>">
			<?php if ($i == 1) : ?>
			<noscript>
				<img src="<?php echo $image['sizes']['16_9_medium']; ?>" />
			</noscript>
			<?php endif; ?>
			<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
		</div>
	<?php $i++;  endforeach; ?>
</div><!-- .single-hero-slider -->
<?php endif;
