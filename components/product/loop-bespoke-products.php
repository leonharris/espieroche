<?php

// Teamplate Part to show Products in the Bespoke Category
// Shown at bottom of Bespoke Page

$args = array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'product_cat' => 'bespoke',
	);
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
	echo '<ul class="products">';
	while ( $loop->have_posts() ) : $loop->the_post();
		wc_get_template_part( 'content', 'product' );
	endwhile;
	echo '</ul>';
} else {
	echo __( 'No products found' );
}
wp_reset_postdata();
