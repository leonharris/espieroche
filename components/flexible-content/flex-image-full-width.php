<?php

// Template for displaying Flexible Page Content - Full Width Image

// get the flexi fields

$thumb_id = get_sub_field('image'); // image id

$small_img = wp_get_attachment_image_src( $thumb_id, '16_9_medium' ); // 767 width
$medium_img = wp_get_attachment_image_src( $thumb_id, '16_9_large' );
$xlarge_img = wp_get_attachment_image_src( $thumb_id, '16_9_xlarge' );
$xxlarge_img = wp_get_attachment_image_src( $thumb_id, '16_9_xxlarge' );

$alt = get_post_meta($thumb_id, '_wp_attachment_image_alt', true);

if ($thumb_id) :
?>
<div class="flexible-image-full-width" data-aos="fade-zoom-in">
	<noscript>
		<img src="<?php echo esc_url($medium_img[0]); ?>" />
	</noscript>
	<img
	src="<?php echo esc_url($small_img[0]); ?>"
    data-sizes="auto"
    data-srcset="<?php echo esc_url($small_img[0]); ?> 767w,
    <?php echo esc_url($medium_img[0]); ?> 1200w,
    <?php echo esc_url($xlarge_img[0]); ?> 1600w,
	<?php echo esc_url($xxlarge_img[0]); ?> 2500w"
	class="lazyload blur-up"
	alt="<?php echo $alt; ?>" />
</div>
<?php endif;
