<?php

// Template for displaying Flexible Page Content - 2 Images with a Text Area

// get the flexi fields

$txt = get_sub_field('text_area');

// image #1
$thumb1_id = get_sub_field('image_1'); // image id
$small1_img = wp_get_attachment_image_src( $thumb1_id, 'medium' );
$medium1_img = wp_get_attachment_image_src( $thumb1_id, 'medium' );
$alt1 = get_post_meta($thumb1_id, '_wp_attachment_image_alt', true);

// image #2
$thumb2_id = get_sub_field('image_2'); // image id
$small2_img = wp_get_attachment_image_src( $thumb2_id, 'medium' );
$alt2 = get_post_meta($thumb2_id, '_wp_attachment_image_alt', true);

// image #3
$thumb3_id = get_sub_field('image_3'); // image id
$small3_img = wp_get_attachment_image_src( $thumb3_id, 'medium' ); // 300px square
$medium3_img = wp_get_attachment_image_src( $thumb3_id, 'medium' ); // 600px sq
$alt3 = get_post_meta($thumb3_id, '_wp_attachment_image_alt', true);


$txt_bg = get_sub_field('text_area_bg_colour');
if (empty($txt_bg)) {
	$bg_col = 'light-grey';
} else {
	$bg_col = $txt_bg;
}

?>

<div class="container flexible flexible-three-img-plus-txt">
	<div class="wrap">

		<?php if ($thumb1_id) : ?>
		<div class="image image-1" data-aos="fade-zoom-in">
			<noscript>
				<img src="<?php echo esc_url($medium1_img[0]); ?>" />
			</noscript>
			<img
			src="<?php echo esc_url($small1_img[0]); ?>"
		    data-sizes="auto"
		    data-srcset="
		    <?php echo esc_url($small1_img[0]); ?> 300w,
		    <?php echo esc_url($medium1_img[0]); ?> 767w"
			class="lazyload blur-up"
			alt="<?php echo $alt1; ?>" />
		</div>
		<?php endif; ?>

		<?php if ($txt) : ?>
		<div class="text-area-wrapper" data-aos="fade-up" data-aos-duration="600" data-aos-anchor-placement="bottom-bottom">
			<div class="panel text-area text-area--bg-<?php echo $bg_col; ?>">
				<?php echo $txt; ?>
			</div>
		</div>
		<?php endif; ?>

		<?php if ($thumb2_id) : ?>
		<div class="image image-2" data-aos="fade-zoom-in">
			<noscript>
				<img src="<?php echo esc_url($small2_img[0]); ?>" />
			</noscript>
			<img
			src="<?php echo esc_url($small2_img[0]); ?>"
		    data-sizes="auto"
		    data-srcset="<?php echo esc_url($small2_img[0]); ?> 300w"
			class="lazyload blur-up"
			alt="<?php echo $alt2; ?>" />
		</div>
		<?php endif; ?>

		<?php if ($thumb3_id) : ?>
		<div class="image image-3" data-aos="fade-zoom-in">
			<noscript>
				<img src="<?php echo esc_url($medium3_img[0]); ?>" />
			</noscript>
			<img
			src="<?php echo esc_url($small3_img[0]); ?>"
		    data-sizes="auto"
		    data-srcset="
		    <?php echo esc_url($small3_img[0]); ?> 300w,
		    <?php echo esc_url($medium3_img[0]); ?> 600w"
			class="lazyload blur-up"
			alt="<?php echo $alt3; ?>" />
		</div>
		<?php endif; ?>

	</div>
</div>
