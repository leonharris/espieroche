<?php

// Template for displaying Flexible Page Content - Quote section

// get the flexi fields

$quote = get_sub_field('quote');

if ($quote) :
?>
<div class="container-fluid flexible-quote">
	<div class="row">
		<div class="text-area-wrapper">
			<div class="text-area">
				<?php echo $quote; ?>
			</div>
		</div>
	</div>
</div>
<?php endif;
