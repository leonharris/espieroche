<?php

// Template for displaying Flexible Page Content - 2 Images with a Text Area

// get the flexi fields

$enlarge_img = get_sub_field('enlarge_img');
$txt = get_sub_field('text_area');

// image #1
$thumb1_id = get_sub_field('image_1'); // image id
$small1_img = wp_get_attachment_image_src( $thumb1_id, 'square-small' ); // 300px square
$medium1_img = wp_get_attachment_image_src( $thumb1_id, 'square-medium' ); // 600px sq
$large1_img = wp_get_attachment_image_src( $thumb1_id, 'square-large' ); // 1200px sq
$alt1 = get_post_meta($thumb1_id, '_wp_attachment_image_alt', true);

// image #2
$thumb2_id = get_sub_field('image_2'); // image id
$small2_img = wp_get_attachment_image_src( $thumb2_id, 'square-small' ); // 300px square
$medium2_img = wp_get_attachment_image_src( $thumb2_id, 'square-medium' ); // 600px sq
$large2_img = wp_get_attachment_image_src( $thumb2_id, 'square-large' ); // 1200px sq
$alt2 = get_post_meta($thumb2_id, '_wp_attachment_image_alt', true);


$txt_bg = get_sub_field('text_area_bg_colour');
if (empty($txt_bg)) {
	$bg_col = 'light-grey';
} else {
	$bg_col = $txt_bg;
}
if ($txt) :
?>
<div class="container flexible-two-img-plus-txt">
	<div class="row enlarge-<?php echo $enlarge_img; ?>-image">
		<div class="image image--left" data-aos="fade-zoom-in">
			<noscript>
				<img src="<?php echo esc_url($medium1_img[0]); ?>" />
			</noscript>
			<img
			src="<?php echo esc_url($small1_img[0]); ?>"
		    data-sizes="auto"
		    data-srcset="
		    <?php echo esc_url($small1_img[0]); ?> 300w,
		    <?php echo esc_url($medium1_img[0]); ?> 600w,
			<?php echo esc_url($large1_img[0]); ?> 1200w"
			class="lazyload blur-up"
			alt="<?php echo $alt1; ?>" />
		</div>
		<div class="text-area-wrapper">
			<div class="text-area text-area--bg-<?php echo $bg_col; ?>" data-aos="fade-up" data-aos-duration="600" data-aos-anchor-placement="bottom-bottom">
				<?php echo $txt; ?>
			</div>
		</div>
		<div class="image image--right" data-aos="fade-zoom-in">
			<noscript>
				<img src="<?php echo esc_url($medium2_img[0]); ?>" />
			</noscript>
			<img
			src="<?php echo esc_url($small2_img[0]); ?>"
		    data-sizes="auto"
		    data-srcset="
		    <?php echo esc_url($small2_img[0]); ?> 300w,
		    <?php echo esc_url($medium2_img[0]); ?> 600w,
			<?php echo esc_url($large2_img[0]); ?> 1200w"
			class="lazyload blur-up"
			alt="<?php echo $alt2; ?>" />
		</div>
	</div>
</div>
<?php endif;
