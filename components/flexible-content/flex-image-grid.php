<?php
$images = get_sub_field('gallery');
$size = 'square-large'; // (thumbnail, medium, large, full or custom size)
$grid_columns = 2; // set default number of grid columns
$grid_columns = get_sub_field('gallery_columns'); // update $grid_columns var if number selected on post

if( $images ): ?>
<div class="flexible flexible-image-grid columns-<?php echo $grid_columns; ?>">
    <div class="wrapper">
        <?php foreach( $images as $image ): ?>
            <div class="image">
            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php endif; ?>
