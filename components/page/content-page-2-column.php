<?php
/**
 * Template part for displaying page content in page.php
 *
 */
?>

<?php

// get the second column content
$col_2 = get_field('column_2');

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">

		<div class="two-col">
			<div class="col-left">
				<?php the_content(); ?>
			</div>
			<div class="col-right">
				<?php echo $col_2; ?>
			</div>
		</div>

		<?php

			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'espieroche' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
