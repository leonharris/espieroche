<?php
/**
 * Template part for displaying page content in page.php
 *
 */
?>
<article <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">

		<div class="entry-intro">
			<?php the_content(); ?>
		</div>

		<?php

		// check if the flexible content field has rows of data
		if( have_rows('flexible_content') ):

		 	// loop through the rows of data
		    while ( have_rows('flexible_content') ) : the_row();

				if( get_row_layout() == 'two_img_and_txt' ):

					get_template_part( 'components/flexible-content/flex', 'two-images-and-text' );

				elseif( get_row_layout() == 'three_img_and_txt' ):

		        	get_template_part( 'components/flexible-content/flex', 'three-images-and-text' );

				elseif( get_row_layout() == 'img_grid' ):

					get_template_part( 'components/flexible-content/flex', 'image-grid' );

		        elseif( get_row_layout() == 'full_width_img' ):

		        	get_template_part( 'components/flexible-content/flex', 'image-full-width' );

				elseif( get_row_layout() == 'quote' ):

					get_template_part( 'components/flexible-content/flex', 'quote' );

		        endif;

		    endwhile;

		else :

		    // no layouts found

		endif;

		?>

		<?php if (is_page('the-brand')) :
			// show the Founders section on Brand Page
			get_template_part( 'components/page/section', 'the-founders' );
		endif; ?>

	</div><!-- .entry-content -->
</article>
