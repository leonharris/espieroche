<?php
/**
 * Displays content for front page
 *
 */
?>
<article <?php post_class( '' ); ?>>

	<div class="container flexible">

		<div class="wrap">

			<?php

			$images = get_field('gallery');

			if( $images ):
				$i = 1; ?>
				<div class="welcome">
			        <?php foreach( $images as $image ): ?>
						<?php if ($i == 2) :
							echo '<div class="entry-content panel">';
							the_content();
							echo '</div><!-- .entry-content -->';
						endif; ?>
			            <div class="image image-<?php echo $i; ?> fade-box">
							<?php if ($i == 2) :
								// get portrait ratio images for image #2
								$small_img = $image['sizes']['medium'];
								$md_img = $image['sizes']['medium'];
							else :
								// get landscape ratio images for the rest
								$small_img = $image['sizes']['thumbnail'];
								$md_img = $image['sizes']['16_9_medium'];
							endif; ?>
							<img
						    data-sizes="auto"
						    src="<?php echo $small_img; ?>"
						    data-srcset="<?php echo $small_img; ?> 300w,
						    <?php echo $md_img; ?> 767w" class="lazyload"
							alt="<?php echo $image['alt']; ?>" />
			            </div>
			        <?php $i++;  endforeach; ?>
				</div>
			<?php endif; ?>

		</div><!-- .wrap -->

	</div><!-- .container -->

	<?php
	$image = get_field('image');

	// vars
	$alt = $image['alt'];

	// the image
	$small_img = $image['sizes'][ '16_9_medium' ]; // 767 width
	$medium_img = $image['sizes'][ '16_9_large' ];
	$xlarge_img = $image['sizes'][ '16_9_xlarge' ];
	$xxlarge_img = $image['sizes'][ '16_9_xxlarge' ];


	$link = get_field('link');
	$link_text = get_field('link_text');

	if( !empty($image) ): ?>
	<div class="fade-box jumbotron-link">
		<img
		data-sizes="auto"
		src="<?php echo $small_img; ?>"
		data-srcset="<?php echo $small_img; ?> 767w,
		<?php echo $medium_img; ?> 1200w,
		<?php echo $xlarge_img; ?> 1600w" class="lazyload"
		alt="<?php echo $alt; ?>"
		/>
		<?php if(!empty($link)) {
			echo '<div class="centered">';
			echo '<a class="btn btn-alt btn-icon--right" href="'.$link.'">'.$link_text.'</a>';
			echo '</div>';
		}
		?>
	</div>
	<?php endif; ?>

</article><!-- #post-## -->
