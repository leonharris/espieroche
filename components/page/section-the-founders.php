<?php
// Temaplate Part showing The Founders
// used on The Brand Page
$founder_img_id = get_field('founders_image');
$founder_txt = get_field('founders_text');
$small_img = wp_get_attachment_image_src( $founder_img_id, '16_9_medium' ); // 767 width
$medium_img = wp_get_attachment_image_src( $founder_img_id, '16_9_large' );
?>
<div class="container-fluid the-founders">
	<div class="row">
		<?php if ($founder_img_id) : ?>
		<div class="image-wrapper" data-aos="fade-zoom-in">
			<img
		    data-sizes="auto"
		    data-src="<?php echo $small_img[0]; ?>"
		    data-srcset="<?php echo $small_img[0]; ?> 767w,
		    <?php echo $medium_img[0]; ?> 1200w"
			class="lazyload blur-up" />
		</div>
		<?php endif; ?>
		<?php if ($founder_txt) : ?>
		<div class="text-area-wrapper">
			<div class="text-area text-area--bg-black">
				<?php echo $founder_txt; ?>
			</div>
		</div>
		<?php endif; ?>

	</div>
</div>
