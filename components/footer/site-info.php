<?php
/**
 * Displays footer site info
 *
 */

?>
<div class="site-info">
	Copyright &copy; <?php echo date('Y'); ?> Espie Roche. All Rights Reserved.
</div><!-- .site-info -->
