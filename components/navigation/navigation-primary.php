<?php
if ( has_nav_menu( 'primary-left' ) ) : ?>
	<nav id="primary-navigation" class="primary-navigation navbar navbar-expand-lg" aria-label="<?php _e( 'Main Menu', 'espieroche' ); ?>">

		<?php
		 wp_nav_menu([
		   'menu'            => 'primary-left',
		   'theme_location'  => 'primary-left',
		   'container'       => 'div',
		   'container_id'    => 'primary-left',
		   'container_class' => 'collapse-lg navbar-collapse justify-content-lg-center',
		   'menu_id'         => false,
		   'menu_class'      => 'primary-menu navbar-nav',
		   'depth'           => 2,
		   'fallback_cb'     => 'bs4navwalker::fallback',
		   'walker'          => new bs4navwalker()
	   ]); ?>

	   <?php if ( is_front_page() ) {
		   echo '<h1 class="home-title">';
	   } ?>
	   <a class="navbar-brand" href="<?php echo home_url(); ?>">
		   <?php bloginfo( 'name' ); ?>
	   </a>
	   <?php if ( is_front_page() ) {
		   echo '</h1>';
	   } ?>

	   <?php
		wp_nav_menu([
		  'menu'            => 'primary-right',
		  'theme_location'  => 'primary-right',
		  'container'       => 'div',
		  'container_id'    => 'primary-right',
		  'container_class' => 'collapse-lg navbar-collapse justify-content-lg-center',
		  'menu_id'         => false,
		  'menu_class'      => 'primary-menu navbar-nav',
		  'depth'           => 2,
		  'fallback_cb'     => 'bs4navwalker::fallback',
		  'walker'          => new bs4navwalker()
	  ]); ?>


	</nav><!-- .navbar -->
<?php endif;
