// Animate on scroll library

AOS.init({
	duration: 500,
	delay: 100,
	easing: 'ease-in-out',
});

jQuery(document).ready(function($) {

	// enable dropdown link click in Bootstrap 4 menu
	// set as the CSS shows the menu on hover
	$('.dropdown-toggle').click(function(e) {
	  if ($(document).width() > 992) { // mobbile menu breakpoint
	    e.preventDefault();
	    var url = $(this).attr('href');
	    if (url !== '#') {
	      window.location.href = url;
	    }
	  }
	});



	/*
	// --------------------------------------------------
	//	Cookie consent
	// 	trigger the div if cookie not present
	*/

	if (jQuery.cookie('cookieConsent') == null) {  // If the cookie has not been set then show the bar
		$("html").addClass("has-cookie-bar");
		//jQuery.cookie('cookieConsent', '1', { expires: 30, path: '/' });
	} else {
	   // console.log('cookie already set');
	}

	// set cookieConsent cookie to 1 on click
	jQuery('#cookieConsent').on('click', function () {
		jQuery.cookie('cookieConsent', '1', { expires: 30, path: '/' });
		$("html").removeClass("has-cookie-bar");
	});


	/*
		Main Navigation
	------------------------------------------------------ */

	var burgerContainer = jQuery(".hamburger")
		navContainer = jQuery('.primary-navigation')
		body = jQuery('body');

	$(burgerContainer).click(function() {

		if ($(this).hasClass("is-active")) {
		  $(this).removeClass("is-active");
		  body.removeClass("menu-active").addClass("menu-inactive");
		  navContainer.removeClass("is-active").addClass("inactive");
		} else {
		  $(this).addClass("is-active");
		  body.removeClass("menu-inactive").addClass("menu-active");
		  navContainer.removeClass("inactive").addClass("is-active");
		}

	});

	/*
		Hero Sliders
		// Slick Slider
	------------------------------------------------------ */
	$('.single-hero-slider').slick({
		arrows : false,
		autoplay: true,
     	//autoplaySpeed: 3000,
		infinite: true,
		speed: 1000,
		fade: true,
		cssEase: 'linear',
		pauseOnHover: false,
      });


	/*
  		Product Archives
		remove Colour name (from brackets) on archive products
  	------------------------------------------------------ */

	$('.woocommerce-loop-product__title').each(function() {
		t = $(this).text();
		//console.log(t);
		s = t.replace(/ *\([^)]*\) */g, "");
		//console.log(s);
		$(this).text(s);
	});


	/*
  		Single Product
		remove Colour name (from brackets) on archive products
  	------------------------------------------------------ */

	$('.single-product .product_title, .single-product .breadcrumb_last').each(function() {
		t = $(this).text();
		//console.log(t);
		s = t.replace(/ *\([^)]*\) */g, "");
		//console.log(s);
		$(this).text(s);
	});

});
