<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 */

?><!doctype html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<a class="skip-link sr-only" href="#content"><?php _e( 'Skip to content', 'espieroche' ); ?></a>

	<header id="masthead" class="site-header" >

		<button class="hamburger hamburger--elastic" type="button" aria-label="Menu" aria-controls="navigation">
		  <span class="hamburger-box">
		    <span class="hamburger-inner"></span>
		  </span>
		</button>

		<div class="helper-menu">
			<div class="left">
				<a href="<?php echo get_home_url(); ?>/contact/">Contact us</a>
			</div>
			<div class="right">
				<ul>
					<?php if( current_user_can('customer') ) {  ?>
					    <li class="login"><a href="<?php echo get_home_url(); ?>/my-account/customer-logout/">Log out</a></li>
					<?php } else { ?>
						<li class="login"><a href="<?php echo get_home_url(); ?>/my-account/">Log in</a></li>
					<?php } ?>
					<li class="woo-cart"><a class="cart-customlocation" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf ( _n( 'Cart: %d', 'Cart: %d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?></a></li>
				</ul>
			</div>
		</div>

		<?php get_template_part( 'components/navigation/navigation', 'primary' ); ?>

		<a href="<?php echo get_home_url(); ?>/" class="home-link"></a>

	</header><!-- #masthead -->


	<?php
	$featured_vid = get_field('featured_video');

	if ( $featured_vid ) :

		// If an image, show it
		get_template_part( 'components/media/hero', 'video' );

	elseif ( has_post_thumbnail() && ( is_single(array('post')) || ( is_page() ) ) ) :

		// If a regular post, page or homepage, show the featured image.
		// If an image, show it
		get_template_part( 'components/media/hero', 'image' );

	else :

		// If a slideshow, show it
		get_template_part( 'components/media/hero', 'slider' );

	endif;
	?>

	<div id="content" class="site-content">

		<?php
		if ( function_exists('yoast_breadcrumb') ) {
		yoast_breadcrumb('
		<nav class="breadcrumb" aria-label="You are here:">','</nav>
		');
		}
		?>
