<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main">

		<?php // Show the selected frontpage content.
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
				get_template_part( 'components/page/content', 'front-page' );
			endwhile;
		else : 
			get_template_part( 'components/post/content', 'none' );
		endif; ?>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer();
