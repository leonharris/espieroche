<?php
/**
 * Description tab
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

// only show if content present
if( '' !== get_post()->post_content ) {

	$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Product details', 'woocommerce' ) ) );
	?>

	<?php if ( $heading ) : ?>
	<h2>
		 <a data-toggle="collapse" href="#collapseDescription" role="button" aria-expanded="false" aria-controls="collapseDescription">
			 <?php echo $heading; ?>
		</a>
	</h2>
	<?php endif; ?>

	<div class="collapse description_content" id="collapseDescription">
		<div class="description_content--inner">
			<?php the_content(); ?>
		</div>
	</div>
<?php
}
